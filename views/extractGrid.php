<?php $this->load->view('template/header');?>
<?php $this->load->view('template/nav');?>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Extract Grid</h3>
  </div>
  <div class="panel-body">
    <?php 
    $tmpl = array ( 'table_open'  => '<table class="table table-bordered table-hover">' );
    $this->table->set_template($tmpl);
    
    echo $this->table->generate($extract);?>
    
  </div>
</div>

<?php $this->load->view('template/footer');?>