 <div class="col-sm-8">
	<a href="<?= base_url('web/complete/'.$this->uri->segment(3));?>" class="btn btn-success active" role="button">Done With Product</a>
	<a href="<?= base_url('web/delProduct/'.$this->uri->segment(3));?>" onclick="return confirm('Are you sure you want to delete this product?');" class="btn btn-danger active" role="button">Delete Product</a>
</div>
<form action="<?= base_url('web/revsubmit');?>" id="entryform" method="POST" class="form-horizontal">
<div class="form-group col-xs-12">
<?php 
foreach($fields->result() as $row){
	$tag=$row->tag; ?>
   <div class="form-group-sm">
    <label class="col-sm-4 control-label" for="formGroupInputSmall"><?php echo character_limiter($row->name,30).':';?></label>
    <div class="col-sm-8">
    	<?php if ($row->type == 1){?> <!-- input text-->
    		<input 	id="<?php echo $row->tag?>"	class="form-control" type="text" name="<?php echo $row->tag?>" value="<?= $scrap->$tag; ?>"/>
      	<?php } if($row->type == 2){  	//dropdown
      		$options = explode(',',$row->atts);
      		?>
      		<select id="<?php echo $row->name?>" class="form-control input-sm" name="<?php echo $row->tag?>">
      			<?php foreach($options as $rowb){?>
      				<option <?php if($row->tag == $rowb){echo 'selected';}?> value="<?php echo $rowb;?>"><?php echo $rowb;?></option>
      			<?php } ?>
      		</select>
      	<?php }?>
      	<?php  if($row->type == 3){  	//dropdown
      		$options = explode(',',$row->atts);?>
      		<?= $scrap->$tag; ?>
      		<select id="<?php echo $row->name?>" class="form-control input-sm" name="<?php echo $row->tag?>">
      			<?php foreach($options as $rowb){?>
      				<option <?php if($row->tag == $rowb){echo 'selected';}?> value="<?php echo $rowb;?>"><?php echo $rowb;?></option>
      			<?php } ?>
      		</select>
      	<?php }?>
		<?php if ($row->type == 4){?> <!-- text area-->
    		<textarea id="<?php echo $row->tag?>" class="form-control" rows="6" type="text" name="<?php echo $row->tag?>" value="<?= $scrap->$tag; ?>"><?= $scrap->$tag; ?></textarea>
      	<?php }?>
	</div>
   </div>
 <?php } ?>
 </div>
 
 	<input type="hidden" name="id" value="<?= $this->uri->segment(3);?>" />
 	<div class="col-sm-4">
 	    
 	</div>
 	
 	
 	<div class="col-sm-8">
 	    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
      View Product Image
    </button>
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Product Image</h4>
          </div>
          <div class="modal-body">
            <img src="<?= $image->img_path;?>" />
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
 		<input id="entrysubmit" type="submit" name="" value="Save" class="btn btn-primary"/>
 	</div>
 </div>
<?= br(3);?>
</form>
<div class="clearfix"></div>

