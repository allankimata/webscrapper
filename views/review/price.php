
<div class="form-group-sm" style="background-color: blue;">
  <?= form_open('web/submitprice',array('class'=>"form-horizontal",'id'=>'priceform'));?>  
    <?php foreach($form2->result() as $row){
    $tag=$row->tag; ?>
    <label class="col-sm-4 control-label" for="amount"><?= $row->name?>:</label>
    <div class="col-sm-8">
        <?php if ($row->type == 1){?> <!-- input text-->
            <input  id="<?= $row->tag?>"  class="form-control" type="text" name="<?= $row->tag?>" />
         <?php } 
        if($row->type == 2){    //dropdown
            $options = explode(',',$row->atts); ?>
            <select id="<?= $row->tag?>" class="form-control input-sm" name="<?= $row->tag?>">
                <?php foreach($options as $rowb){?>
                    <option <?php if($row->tag == $rowb){echo 'selected';}?> value="<?= $rowb;?>"><?= $rowb;?></option>
                <?php } ?>
            </select>
        <?php }?>
        <?php if($row->type == 3){     //dropdown
            $options = explode(',',$row->atts);?>
            <?= $scrap->$tag; ?>
            <select id="<?php echo $row->name?>" class="form-control input-sm" name="<?php echo $row->tag?>">
                <?php foreach($options as $rowb){?>
                    <option <?php if($row->tag == $rowb){echo 'selected';}?> value="<?php echo $rowb;?>"><?php echo $rowb;?></option>
                <?php } ?>
            </select>
        <?php }?>
        <?php if ($row->type == 4){?> <!-- text area-->
            <textarea id="<?php echo $row->tag?>" class="form-control" rows="6" type="text" name="<?php echo $row->tag?>" value=""></textarea>
        <?php }?>
    </div>
    <?php } ?>
    
    <input type="hidden" name="item_id" id="item_id" value="<?= $this->uri->segment(3);?>" />
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
        <input id="pricesubmit" type="submit" name="" value="Submit Size/ Shade/ Price" class="btn btn-primary"/>
    </div>
    </form>
</div>
