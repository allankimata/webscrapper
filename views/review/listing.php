
<div class="fluid">
<div class="panel panel-primary" style="height: 900px; overflow-y: auto;">
  <!-- Default panel contents -->
  <div class="panel-heading">PRODUCTS COMPLETED</div>
  <div class="panel-body">
    <table class="table table-hover">
        <th>#</th>
        <th>Name</th>
        <th>Product ID</th>
        <th>UPC</th>
        <th>UPC Source</th>
        <th>ASIN</th>
        <th>Size</th>
        <th>Price</th>
        <th>Description</th>
        <th>Ingredients</th>
        <th>Edit</th>
    <?php $i=1; foreach($products as $row){?>
        <tr><td><?= $i;?></td>
            <td><?= $row->prod_name;?></td>
            <td><?= $row->prod_id?></td>
            <td><?= $row->upc?></td>
            <td><?= $row->upc_src?></td>
            <td><?= $row->asin?></td>
            <td><?= $row->size?></td>
            <td><?= $row->price?></td>
            <td><?= $row->prod_desc?></td>
             <td><?=$row->ingredients?></td>
            <td><a href="<?= base_url('web/review/'.$row->id);?>" ><span class="glyphicon glyphicon-pencil"></span></a></td>
        </tr>
    <?php $i++; }?>
    
    </table>
  </div>

</div>
</div>