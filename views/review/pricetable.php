<div class="col-md-12">
	<table class="table table-bordered">
	  <tr>
	  	<th>Size</th>
	  	<th>Price</th>
	  	<th>Product ID</th>
	  	<th>Shade</th>
	  	<th>UPC</th>
	  	<th>UPC Source</th>
	  	<th>ASIN</th>
	  	<th>EDIT</th>
	  	<th>DELETE</th>
	  </tr>
	  <?php foreach($pricedata as $price){?>
	  <tr>
	  	<td><?= $price->size;?></td>
	  	<td><?= $price->price;?></td>
	  	<td><?= $price->prod_id;?></td>
	  	<td><?= $price->shade;?></td>
	  	<td><?= $price->upc;?></td>
	  	<td><?= $price->upc_src;?></td>
	  	<td><?= $price->asin;?></td>
	  	
	  	<td><?= anchor('web/delprice/'.$price->id.'/'.$this->uri->segment(3),'<span class="glyphicon glyphicon-trash">');?></td>
	  	<td>
	  	    <a href="#" data-toggle="modal" data-target="#<?= $price->id;?>Modal"><span class="glyphicon glyphicon-pencil" ></span></a>
	  	    </td>
	  </tr>
	  
        <!-- Modal -->
        <div class="modal fade" id="<?= $price->id;?>Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit <?= $price->id;?></h4>
              </div>
              <div class="modal-body">
                 <?= form_open('web/editProdData',array('class'=>"form-horizontal",'id'=>'priceform'));?>  
                    <?php foreach($form2->result() as $row){
                    $tag=$row->tag; ?>
                    <label class="col-sm-4 control-label" for="amount"><?= character_limiter($row->name,20);?>:</label>
                    <div class="col-sm-8">
                        <?php if ($row->type == 1){?> <!-- input text-->
                            <input  id="<?= $row->tag?>"  class="form-control" type="text" name="<?= $row->tag?>" value="<?= $price->$tag;?>" />
                         <?php } 
                        if($row->type == 2){    //dropdown
                            $options = explode(',',$row->atts); ?>
                            <select id="<?= $row->tag?>" class="form-control input-sm" name="<?= $row->tag?>">
                                <?php foreach($options as $rowb){?>
                                    <option <?php if($row->tag == $rowb){echo 'selected';}?> value="<?= $rowb;?>"><?= $rowb;?></option>
                                <?php } ?>
                            </select>
                        <?php }?>
                        <?php  if($row->type == 3){     //dropdown
                            $options = explode(',',$row->atts);?>
                            <?= $scrap->$tag; ?>
                            <select id="<?php echo $row->name?>" class="form-control input-sm" name="<?php echo $row->tag?>">
                                <?php foreach($options as $rowb){?>
                                    <option <?php if($row->tag == $rowb){echo 'selected';}?> value="<?php echo $rowb;?>"><?php echo $rowb;?></option>
                                <?php } ?>
                            </select>
                        <?php }?>
                        <?php if ($row->type == 4){?> <!-- text area-->
                            <textarea id="<?php echo $row->tag?>" class="form-control" rows="6" type="text" name="<?php echo $row->tag?>" value="<?= $price->$tag; ?>"><?= $price->$tag; ?></textarea>
                        <?php }?>
                        
                        
                    </div>
                    <?php } ?>
              <input type="hidden" name="price_id" id="price_id" value="<?= $price->id;?>" />
                        <input type="hidden" name="item_id" id="item_id" value="<?= $this->uri->segment(3);?>" />
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              <?= form_close();?>
            </div>
          </div>
        </div>
	  
	  <?php }?>
	</table>
	
	
</div>