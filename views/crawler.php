<?php $this->load->view('template/header');?>
<?php $this->load->view('template/nav');?>
   

    <!-- Page Content -->
   

        <div class="row">
		  	<div class="col-md-7">
				<div class="panel panel-primary">
				  <div class="row">
				  	<div class="col-md-2">Image</div>
				  	<div class="col-md-10"><img src="<?= $image;?>" width="20%" /></div>
				  </div>
				  <div class="row">
				  	<div class="col-md-2">Price</div>
				  	<div class="col-md-10"><?= $price;?></div>
				  </div>
				  <div class="row">
				  	<div class="col-md-2">Name:</div>
				  	<div class="col-md-10"><?= $name;?></div>
				  </div>
				  <div class="row">
				  	<div class="col-md-2">Collection:</div>
				  	<div class="col-md-10"><?= $collection;?></div>
				  </div>
				   <div class="row">
				  	<div class="col-md-2">Movement Info:</div>
				  	<div class="col-md-10"><?php foreach($movement as $r) {if(!is_string($r)){echo $r;}}?></div>
				  </div>
				</div>
			</div>
			
		  	<div class="col-md-5">
				<div class="panel panel-success">
				  <div class="panel-heading">
				    <h3 class="panel-title">Panel title</h3>
				  </div>
				  <div class="panel-body">
				    Panel content
				  </div>
				</div>
			</div>
			  
		</div>
        <!-- /.row -->

    
    <!-- /.container -->
<?php $this->load->view('template/footer');?>