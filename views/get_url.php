<?php $this->load->view('template/header');?>
<?php $this->load->view('template/nav');?>
<style type="text/css">
	input[type=checkbox] + label {
	  color: #ccc;
	  font-style: italic;
	} 
	input[type=checkbox]:checked + label {
	  color: #f00;
	  font-style: normal;
	} 
</style>

  <div class="container">
  <div class="panel panel-default">
  	
  	<div class="panel-heading">Enter Caregory URL below</div>
  	<div class="panel-body">
  	<?= form_open('web/geturls',array('class'=>'form-inline'));?>
	  
	  	<div class="form-group" style="width:600px;">
	    	<label class="sr-only" for="link">URL</label>
	    	<input type="text" style="width:100%" class="form-control" id="link" name="url" placeholder="Enter URL">
	 	</div>
	  <button type="submit" class="btn btn-default">Submit</button>
	 </form>
  	</div>
 
  	
  	<?php if(!empty($urls)){?>
	  <div class="panel-heading">Select and Submit Relevant Links To Scrape</div>
	  <div class="panel-body">
	    <div class="row" style="padding:0 20px">
	    
	    <?= form_open('web/crawler');$i=1?>
	     <div class="form-group">
	  	<select name="web_id" class="form-control">
	  		<option>Select Website</option>
	  		<?php foreach($website as $val){?>
	  			<option value="<?= $val->id;?>"><?= $val->website;?></option>
	  		<?php }?>
	  	</select>
	  	</div>
	  	<div style="overflow-y:scroll;height: 400px;">
	  		<?php foreach (array_unique($urls) as $r){?>
	  		<div class="checkbox">
			  
			  <label class="label-for-check">
			  	 <input name="urls<?= $i;?>" class="check-with-label" type="checkbox" value="<?= $r;?>">
			  	<?= $r;?>
			 </labe>
			</div>
	  		<?php $i++; }?>
	  	</div>
	  	
	  	<?= form_submit('','Sumbit Selected URLS');?>
	  	</form>
	  </div>
	  </div>
	 <?php }?>
	</div>

  
  </div>
<?php $this->load->view('template/footer');?>