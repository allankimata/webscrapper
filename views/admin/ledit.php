<?php $this -> load -> view('template/header'); ?>
<?php $this -> load -> view('template/nav'); ?>
<script>
    $('.editable').each(function(){
    this.contentEditable = true;
});
</script>
<style type="text/css">div.editable {
 
    height: 20px;
    border: 1px solid #ccc;
    padding: 5px;
}</style>
<div class="container">
 <div class="panel panel-primary">
  <div class="panel-heading"><?php $d=$this->db->get_where('websites',array('id'=>$webid))->row(); echo $d->website;?> Limits</div>
  <div class="panel-body" >
      <div class="list-group fluid">
          <?= form_open('funk/doeditlimits','class="form-horizontal"');?>
          <?php foreach($info as $row){ ?>
              <div class="form-group">
                <label for="<?= $row->tag?>" class="col-sm-3 control-label"> <?= $row->name;?></label>
                <div class="col-sm-3"><code><?= $row->start;?></code></div> 
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="start_<?= $row->field_id?>" value=""/>
                </div>
                <div class="col-sm-1"><code><?= $row->end;?></code></div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="end_<?= $row->field_id?>" value=""/>
                </div>
               </div>
          <?php } ?>
          <?= form_close();?>
        </div>
  </div>
</div>   

</div>

<?php $this -> load -> view('template/footer'); ?>