<?php $this -> load -> view('template/header'); ?>
<?php $this -> load -> view('template/nav'); ?>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Data View - Select Website to view</h3>
  </div>
  <div class="panel-body" style="height:900px; overflow-y: auto;">
      
<?= form_open('funk/getScrapData','class="form-group"');?>
    <select name="website" class="form-control" onChange="this.form.submit();" >
        <?php foreach ($brands as $value) {?>
            <option value="<?= $value->wid?>"><?= $value->website;?></option>
        <?php }?>
    </select>
<?= form_close();?>
<?php if($ch ==1){?>
<div class="table-responsive">
  <table class="table table-bordered">
    <tr>
        <th>#</th>
        <th>Product Name</th>
        <th>Product ID</th>
        <th>UPCs </th>
        <th>Validation Source for UPC</th>
        <th>Amazon ASIN</th>
        <th>Product Description</th>
        <th>Brand Name</th>
        <th>Size </th>
        <th>Price </th>
        <th>Image Path</th>
        <th>Ingredient List [required]</th>
        
    </tr>
    <?php $i=1; foreach ($data as $row) { ?>
        <tr>
        <td><?= $i;?></td>  
        <td><?= $row->prod_name;?></td>
        <td><?= $row->prod_id;?></td>
        <td><?= $row->upc;?></td>
        <td><?= $row->upc_src;?></td>
        <td><?= $row->asin;?></td>
        <td><?= $row->prod_desc;?></td>
        <td><?= $row->brand;?></td>
        <td><?= $row->size;?></td>
        <td><?= $row->price;?></td>
        <td><?= $row->img_path;?></td>
        <td><?= $row->ingredients;?></td>


        </tr>
    <?php $i++; } ?>

  </table>
</div>
<?php }?>
 </div>
</div>
<?php $this -> load -> view('template/footer'); ?>

