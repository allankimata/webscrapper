<?php $this -> load -> view('template/header'); ?>
<?php $this -> load -> view('template/nav'); ?>
<div class="container">
<?= form_open('funk/addBrand', array('class' => "form-horizontal")); ?>
  <div class="form-group">
    <label for="website" class="col-sm-2 control-label">Brand Name</label>
    <div class="col-sm-10">
      <input type="text" name="website" required="required" class="form-control" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="website" class="col-sm-2 control-label">Base URL</label>
    <div class="col-sm-10">
      <input type="text" name="url" class="form-control" placeholder="">
    </div>
  </div>
  <input type="hidden" name="proj_id" value="<?= $this->session->userdata('project');?>" />
  <!-- <div class="form-group">
    <label for="website" class="col-sm-2 control-label">Connected</label>
    <div class="col-sm-10">
      <select class="form-control"  name="connected">
          <option value="0">No</option>
          <option value="1">Yes</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="website" class="col-sm-2 control-label">Product Type</label>
    <div class="col-sm-10">
      <select class="form-control" name="product_type">
          <?php foreach($product_type as $row){?>
            <option value="<?= $row -> type; ?>"><?= $row -> type; ?></option>
          <?php } ?>
        </select>
    </div>
  </div> -->
 
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Add Brand</button>
    </div>
  </div>
</form>

<div class="table-responsive" style="overflow:auto;height: 500px;">
  <table class="table">
      <th>#</th>
      <th>Brand Name</th>
      <th>url</th>
      <th>Edit</th>
      <th>Delete</th>
    <?php foreach($brands as $row){
        switch ($row->active) {
            case '1':  $act = 'warning'; 
            break;
            case '0':  $act = 'info'; 
            break;
            case '2':  $act = 'success'; 
            break;
            default:  $act = 'danger'; 
            break;
        }?>
    <tr class="<?= $act; ?>">
        <td><?= $row -> id; ?></td>
        <td><?= $row -> website; ?></td>
        <td><?= $row -> url; ?></td>
        <td><a data-toggle="modal" data-target="#myModal<?= $row -> id; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
        <td><a href="<?= base_url('funk/deletebrand/'.$row -> id);?>" onclick="return confirm('Are you sure you want to delete this brand?');"><span class="glyphicon glyphicon-trash"></span></a></td>
    </tr>
    
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal<?= $row -> id; ?>">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit <?= $row -> website; ?></h4>
          </div>
          <?= form_open('funk/editbrand');?>
          <div class="modal-body">
              <div class="form-group">
                <label for="website" class="col-sm-4 control-label">Brand Name</label>
                <div class="col-sm-8">
                  <input type="text" name="website" required="required" class="form-control" value="<?= $row -> website; ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="website" class="col-sm-4 control-label">Base URL</label>
                <div class="col-sm-8">
                  <input type="text" name="url" class="form-control" value="<?= $row -> url; ?>">
                </div>
              </div>
              <input type="hidden" name="brand_id" value="<?= $row -> id; ?>" />
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
          <?= form_close();?>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <?php } ?>
    </table>
</div>
</div>



<?php $this -> load -> view('template/footer'); ?>