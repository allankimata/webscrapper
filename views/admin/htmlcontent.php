<?php $this->load->view('template/header');?>
<?php $this->load->view('template/nav');?>
<style type="text/css">
    #snippet-container {
    white-space: pre-wrap;
    white-space: -moz-pre-wrap; 
    white-space: -pre-wrap;
    white-space: -o-pre-wrap;
    word-wrap: break-word;
}
</style>

<div class="container" >
<?= form_open('funk/gethtml',array('class'=>'form-inline'));?>
    <div class="form-group" style="width:600px;">
        <label class="sr-only" for="product_url">URL</label>
        <input type="text" style="width:100%" class="form-control" id="product_url" name="product_url" placeholder="Enter Product URL">
    </div>
    <button type="submit" class="btn btn-default">Scrap URL</button>
</form>

<div class="panel panel-default">
  <div class="panel-body" style="overflow-y: scroll; height:400px;">
    <xmp id="snippet-container"><?= $pagesource;?></xmp>
   </div>
</div>

<?= form_open('funk/set_limit',array('class'=>'form-inline'));?>
<div class="col-md-12">
<label class="sr-only" for="website">Select Website</label>    
<select class="form-control" name="website">
  <?php foreach($websites as $row){?>
    <option value="<?= $row->id;?>"><?= $row->website;?></option>
  <?php }?>
</select>
</div>
<div class="col-md-12">
<?php $i=1; 
      $split = $fields->num_rows()/2;  
      foreach($fields->result() as $row){?>
        
            <div class="row" <?php if($i%2 !=0){echo ' style="background-color:lavender;"';}?>>
              <div class="col-md-6 "><?= $row->name;?>:</div>
              <div class="col-md-3"><input type="text" name="start_<?= $row->id;?>" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_<?= $row->id;?>" class="form-control" placeholder=" End"></div>
            </div>
        
        
<?php $i++; } ?>
            <!-- <div class="row">
              <div class="col-md-6 ">Image 1:</div>
              <div class="col-md-3"><input type="text" name="start_img_1" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img1" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 2:</div>
              <div class="col-md-3"><input type="text" name="start_img_2" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img2" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 3:</div>
              <div class="col-md-3"><input type="text" name="start_img_3" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img3" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 4:</div>
              <div class="col-md-3"><input type="text" name="start_img_4" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img4" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 5:</div>
              <div class="col-md-3"><input type="text" name="start_img_5" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img5" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 6:</div>
              <div class="col-md-3"><input type="text" name="start_img_6" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img6" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 7:</div>
              <div class="col-md-3"><input type="text" name="start_img_7" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img7" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 8:</div>
              <div class="col-md-3"><input type="text" name="start_img_8" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img8" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 9:</div>
              <div class="col-md-3"><input type="text" name="start_img_9" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img9" class="form-control" placeholder=" End"></div>
            </div>
            <div class="row">
              <div class="col-md-6 ">Image 10:</div>
              <div class="col-md-3"><input type="text" name="start_img_10" class="form-control" placeholder="Start"></div>
              <div class="col-md-3"><input type="text" name="end_img10" class="form-control" placeholder=" End"></div>
            </div> -->
</div>
    <div class="col-md-12"><button type="submit" class="btn btn-success">Set Scrape Limits</button></div>
    
    </form>
</div>
<?php $this->load->view('template/footer');?>