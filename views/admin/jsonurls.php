<?php $this->load->view('template/header');?>
<?php $this->load->view('template/nav');?>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">CSV Upload</div>
        <div class="panel-body">
        <div class="form-group">
                <?= form_open_multipart('funk/csvupload','class="form-group"');?>
               <div class="form-group">
               <label for="website"> Select Website:</label>
               <select name="website" class="form-control">
               <?php foreach ($websites as $value) {?>
                   <option value="<?= $value->id?>"><?= $value->website;?></option>
               <?php }?>
               </select>
               </div>
                <label for="csv"> Select CSV file to upload</label>
                <input type="file" class="form-control" name="userfile" id="userfile"  align="center"/>
                <input type="submit" class="btn btn-success" value="Upload"/>
               <?= form_close();?>
        </div>
    </div>
    
</div>
<?php $this->load->view('template/footer');?>