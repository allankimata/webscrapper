   <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            	
                <nav class="navbar navbar-default">
				  <div class="container-fluid">
				   <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				      	<li ><?= anchor('',PRJNAME.' Scrapping')?></li>
				      	<?php if ($this->session->userdata('ul_id')==2){?>
				        <li class="<?php if($menu==1){echo "active";}?>"><a href="<?= base_url('funk/brands');?>">Brands</a></li>
				       
				        
				        <li class="dropdown <?php if($menu==8 || $menu==2){echo "active";}?>">
                          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Limits<span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li class="<?php echo $menu==2 ? "active" :FALSE ;?>"><a href="<?= base_url('funk/limits');?>">Define Limits</a></li>
                            <li class="<?php echo $menu==8 ? "active" :FALSE ;?>"><a href="<?= base_url('funk/deflimits');?>">Edit Defined Limits</a></li>
                          </ul>
                        </li>
				        
				        
				        <li class="dropdown <?php if($menu==3 || $menu==4 || $menu==9){echo "active";}?>">
                          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">URLs<span class="caret"></span></a>
                          <ul class="dropdown-menu">
                              
                            <li class="<?php if($menu==3){echo "active";}?>"><a href="<?= base_url('funk/json_urls');?>">URLs Upload</a></li>
                            <li class="<?php if($menu==4){echo "active";}?>"><a href="<?= base_url('web/geturls');?>">Get Urls</a></li>
                            <li class="<?php if($menu==9){echo "active";}?>"><a href="<?= base_url('funk/getScrapData');?>">Check Scrapped Data</a></li>
                            
                          </ul>
                        </li>
				        <li class="<?php if($menu==5){echo "active";}?>"><a href="<?= base_url('web');?>">Review Scrapped Data</a></li>
                        <!-- <li class="<?php if($menu==6){echo "active";}?>"><a href="<?= base_url('funk/admin_functions');?>">Vital Functions</a></li>
				         -->
				        <?php }?>
				        
				     </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				          <li class="<?php if($menu==10){echo "active";}?>"><?= anchor('web/extractgrid','Extract Grid');?></li>
				          <li class="<?php if($menu==7){echo "active";}?>"><?= anchor('web/completed','Products Completed');?></li>
                        
				      		<li class="active"> <a><?= $this->session->userdata('ul_name').' | '.$this->session->userdata('stf_std_id');?></a></li>
				           <li class="dropdown">
				          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= $this->session->userdata('user');?> <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				              <li> <a href="#">Today's Productivity: <?= $daysProductivity->cnt; ?></a></li>
				            <li><a href="<?= base_url('users/logout')?>">Logout</a></li>
				            
				          </ul>
				        </li>
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
                    
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>