<?php
/**
 * 
 */
class Review_model extends CI_Model {
	protected $prjTbl;
    
    function __Construct()
    {
        parent::__Construct();
        $this->prjTbl = PRJTBL;
    }
	function getData($id) {
		$this->db->query('UPDATE '.$this->prjTbl.' SET opened=1 ,opened_by=? WHERE id =?',array($this->session->userdata('username'),$id));
		$query = $this->db->query('SELECT * FROM '.$this->prjTbl.' JOIN ulrs ON '.$this->prjTbl.'.url_id=ulrs.id JOIN websites ON websites.website='.$this->prjTbl.'.brand WHERE '.$this->prjTbl.'.id=?',$id)->row();
		return $query;
	}
	
	function getFields($connected){
		$this->db->where(array('active'=>1,'form'=>1));
		
		$num = $this->db->get_where('fields',array('project_id'=>2))->num_rows();
		$qry = $this->db->query('SELECT * FROM fields WHERE active = 1 AND project_id=? AND project_id=2 and form=1 ORDER BY ord ASC',$this->session->userdata('project'));
		
		return $qry;
	}
	
	function getWeb(){
	    $this->db->order_by('id','DESC');
		$query = $this->db->get_where('websites',array('active'=>1,'proj_id'=>$this->session->userdata('project')))->result();
		return $query;
	}
	function getCurr(){
		$query = $this->db->get('currency')->result();
		return $query;
	}
	function getPrice($id){
		$query = $this->db->query('SELECT * FROM '.$this->prjTbl.'_price  WHERE '.$this->prjTbl.'_price.item_id = ?',$id)->result();
		return $query;
	}

	function getReport(){
		$qry = $this->db->query("SELECT brand AS 'Brand',count(id) as 'Products Remaining' FROM ".$this->prjTbl." WHERE reviewed=0 AND deleted = 0 GROUP BY brand");
		return $qry;
	}
    
    function getForm2(){
        return $this->db->where(array('active'=>1,'form'=>2,'project_id'=>$this->session->userdata('project')))
                        ->order_by('ord')
                        ->get('fields');
    }

    function getProductivity(){
        return $this->db->query('SELECT count(g.id) AS cnt  FROM '.$this->prjTbl.' g LEFT JOIN `'.$this->prjTbl.'_price` `gp` ON ((`g`.`id` = `gp`.`item_id`)) WHERE DATE(saangapi) = CURDATE() AND rev_by = ?',$this->session->userdata('username'))->row();
    }
    public function getImg($id)
    {
        return $this->db->select('img_path')->get_where(''.$this->prjTbl.'',array('id'=>$id))->row();
    }
    
    public function getCompleted()
    {
           $qry = $this->db->query('call pr_getcompleted(?)',$this->session->userdata('username'));
           mysqli_next_result($this->db->conn_id);
           return $qry->result();
        //return $this->db->query('SELECT * FROM '.$this->prjTbl.' WHERE rev_by="lydia.kaol" AND deleted = 0 AND extracted = 0 ORDER BY id DESC',$this->session->userdata('username'))->result();
    }
}
