<?php 
/**
 * 
 */
class Crawl_model extends CI_Model {
	
	function getLimits($webId){
	    $qry = $this->db->query('SELECT * FROM limits JOIN `fields` ON limits.field_id =`fields`.id 
		                              WHERE limits.web_id=? AND limits.active=1 AND limits.type=1 AND `fields`.project_id=?
		                                 ',array($webId,$this->session->userdata('project')) )->result();
		return $qry;
	}
	
	function getAttrib($webId){
		$qry = $this->db->query('SELECT * FROM limits JOIN `fields` ON limits.field_id =`fields`.id WHERE limits.web_id=? AND limits.active=1 AND limits.type=3 AND `fields`.project_id=?
                                         ',array($webId,$this->session->userdata('project')))->result();
		return $qry;
	}
	
	function getImage($webId){
		$qry = $this->db->query('SELECT * FROM limits JOIN `fields` ON limits.field_id =`fields`.id WHERE limits.web_id=? AND limits.active=1 AND limits.type=2 AND `fields`.project_id=?
                                         ',array($webId,$this->session->userdata('project')))->result();
		return $qry;
	}
}
