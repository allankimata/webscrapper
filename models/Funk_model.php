<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Funk_model extends CI_Model {
    
    protected $table_name;
    
    public function __construct() {
        parent::__construct();
       $this->table_name = 'fields';
    }
    
    public function getFields() {
        return $this->db->from($this->table_name)->where(array('active !='=>'0','form'=>'1','project_id'=>$this->session->userdata('project')))->get();
    }
    
    public function getWebsites(){
        return $this->db->where(array('active'=>1,'proj_id'=>$this->session->userdata('project')))->order_by('id','DESC')->get('websites')->result();
    }
    
    public function getBrands(){
        return $this->db->query("SELECT * FROM websites WHERE proj_id=? ORDER BY FIELD(active,'1','2','0'), id DESC",PRJID)->result();
    }
    
    function getScrappedBrands()
    {
        return $this->db->query('SELECT w.id as wid, w.proj_id,w.website,w.url,w.active,p.* FROM websites w JOIN '.PRJTBL.' p ON w.website = p.brand WHERE proj_id=? GROUP BY p.brand',PRJID)->result();
    }
    
    public function getBrandsWithLimits()
    {
        return $this->db->query("SELECT l.web_id,w.website FROM limits l JOIN websites w ON l.web_id = w.id WHERE w.proj_id=? GROUP BY web_id",PRJID)->result();
    }
    
    function getProductType(){
        return $this->db->get('product_type')->result();
    }
    
    public function uploadCsvData()
    {
         $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
               while($csv_line = fgetcsv($fp,1024)) 
               {
                 for ($i = 0, $j = count($csv_line); $i < $j; $i++) 
                   {
                    $insert_csv = array();
                    $insert_csv['url'] = $csv_line[0];
                   }
                 
                   $data = array(
                    'url' => $insert_csv['url'] ,
                    'website_id' => $this->input->post('website')
                    
                    );
                   $data['str']=$this->db->insert('links', $data);
              }
           fclose($fp) or die("can't close file");
         redirect('web/jsoncrawler/'.$this->input->post('website'));
       
    }
    
    public function editBrand()
    {
        $id = $this->input->post('brand_id');
        $data = $this->input->post();
        unset($data['brand_id']);
        $this->db->where('id',$id);
        $this->db->update('websites',$data);
    }
    
    public function getData()
    {
        return $this->db->query('SELECT
                    `g`.`prod_name` AS `prod_name`,
                    `g`.`prod_id` AS `prod_id`,
                    `g`.`upc` AS `upc`,
                    `g`.`upc_src` AS `upc_src`,
                    `g`.`asin` AS `asin`,
                    `g`.`prod_desc` AS `prod_desc`,
                    `g`.`brand` AS `brand`,
                    `g`.`size` AS `size`,
                    `g`.`price` AS `price`,
                    `g`.`img_path` AS `img_path`,
                    `g`.`ingredients` AS `ingredients`,
                    `g`.`prod_url` AS `prod_url`,
                    `g`.`shade` AS `shade`,
                    `l`.`start` AS `start`,
                    `l`.`end` AS `end`,
                    `f`.`name` AS `name`,
                    `f`.`tag` AS `tag`,
                    `w`.`id` AS `id`,
                    `w`.`proj_id` AS `proj_id`, `w`.`website` AS `website`
                
                FROM
                    goodguides g
                JOIN websites w ON g.brand = w.website
                JOIN limits l ON w.id = l.web_id
                JOIN `fields` f ON l.field_id=f.id
                WHERE
                    w.id = ?',$this->input->post('website'))->result();
    }
 
}