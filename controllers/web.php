<?php
ini_set('MAX_EXECUTION_TIME', -1);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('review_model','crawl_model'));
		$this->require_login();
        $this->prjTbl = PRJTBL;
	}
	
	public function require_login(){
		if ($this->session->userdata('logged_in')==TRUE){
			return TRUE;
		}
		else{
			redirect('users');
		}
	}
	////////////////////////////
	function ifNewUser(){
		$query1 = $this->db->query('SELECT id FROM '.$this->prjTbl.' WHERE opened_by = ?',$this->session->userdata('username'));//if new user or ever done a record
		if ($query1->num_rows()<1){
			$data1 = $this->db->query('SELECT min(id) as id FROM '.$this->prjTbl.' WHERE opened_by = ? and reviewed=0',$this->session->userdata('username'));
			$res = $data1->row();
			$num = $data1->num_rows();
			if($num > 0 && $res->id != NULL){
				
				$last_opened = $res->id;
				redirect('web/review/'.$last_opened);
			}
			else{
			
				$data = $this->db->query('SELECT min(id) as id FROM open')->row();
				$next = $data->id;
				redirect('web/review/'.$next);
			}
		}
   		else {
   			return TRUE;
   		}
	}
	
	function ifAllImagesLocked(){
		$q2 = $this->db->query('SELECT id FROM open');
		if ($q2->num_rows()> 0 ){//checks whether all files have been done
			return TRUE;
		}
		else{
			$data = array(
                        'report'=>$this->review_model->getReport(),
                        'daysProductivity'=>$this->review_model->getProductivity(),
                       'menu'=>8 );
        $this->load->view('complete',$data);
		}
	}
	function ifDoneByCurrUsr($id){
		$query = $this->db->query('SELECT rev_by FROM '.$this->prjTbl.' WHERE id = ?',$id)->row();
		if($query->rev_by == $this->session->userdata('username')){
			return TRUE;
		}
		elseif($query->rev_by == NULL){
			return TRUE;
		}
		elseif ($query->rev_by != $this->session->userdata('username') && $query->rev_by != NULL) {
		   $data = array('report'=>$this->review_model->getReport(),
                        'daysProductivity'=>$this->review_model->getProductivity(),
                       'menu'=>7 );
		   $this->load->view('not_authorized',$data);
		      
		}
		
	}

	function index(){
	 	$this->ifAllImagesLocked();
		$this->ifNewUser();
			$data1 = $this->db->query('SELECT min(id) as id FROM '.$this->prjTbl.' WHERE deleted=0 AND opened_by = ? and reviewed=0',$this->session->userdata('username'));
			$err = $data1->row();
			if($err->id != NULL){
				$id = $data1->result();
				$last_opened = $id[0]->id;
				redirect('web/review/'.$last_opened);
			}
			else{
				redirect('web/next');
			}
	}
	
	 function next(){
		$this->ifAllImagesLocked();
		$this->ifNewUser();
			
			$data1 = $this->db->query('SELECT min(id) as id FROM '.$this->prjTbl.' WHERE deleted=0 AND opened_by = ? and reviewed=0',$this->session->userdata('username'));
			$res = $data1->result();
			$num = $data1->num_rows();
			if($num > 0 && $res[0]->id != NULL){
				
				$last_opened = $res[0]->id;
				redirect('web/review/'.$last_opened);
			}
			else{
			
				$data = $this->db->query('SELECT min(id) as id FROM open')->result();
				$next = $data[0]->id;
				redirect('web/review/'.$next);
			}
	}
	
	function complete($id){
		$this->db->query('UPDATE '.$this->prjTbl.' SET reviewed = 1,rev_by=? WHERE id = ?',array($this->session->userdata('username'),$id));
		redirect('web/next');
	}
	
	////////////////////////////

	public function crawler()
    {
    	$webId = $this->input->post('web_id');//get the website ID
		$postData = $this->input->post(); //assign post data to array $postData
		$limits = $this->crawl_model->getLimits($webId);
		$atrributes = $this->crawl_model->getAttrib($webId);
		$image = $this->crawl_model->getImage($webId);
		$webName = $this->db->query('SELECT url,website FROM websites WHERE id = ?',$webId)->row();
		
		unset($postData['web_id']);
				
    	foreach ($postData as $key => $value) {
    			
    		$url = $webName->url.$value;
			
            if (!$data = file_get_contents($url)){
               $this->db->query("INSERT INTO `ulrs`( `web_id`,`str`, `status`) VALUES (?,?,?)",array($webId,$url,2));
            }
            else {
            
    			$html = file_get_html($url);
    			$html0 =preg_replace('/[ ]{2,}|[\t]/',"",str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', '',$html));
    			
               
                	
    			$insData = array();
    			$insItemData = array();
    			foreach ($limits as $r) {
    				if(strpos($html0, $r->start) !==FALSE){
    				    if($r->form == 1){
    					   $insData[$r->tag]=$this->GetBetween($r->start,$r->end,$html0);
                        }
                        if($r->form == 2){
                           $insItemData[$r->tag]=$this->GetBetween($r->start,$r->end,$html0);
                        }
                        
    				}
    			}
    			foreach ($image as $r) {
    				foreach($html->find('img#'.$r->tag) as $element)
    		       		$insData[$r->tag]= $element->src;
    			}
    			
    			foreach($html->find('title') as $element)
    	       		$title= str_replace('</title>','',str_replace('<title>', '', $element));
    		  	
    		    $this->db->query("INSERT INTO `ulrs`(`web_id`, proj_id,`str`,`pagetitle`, `status`, `urlhtml`) VALUES (?,?,?,?,?,?)",array($webId,PRJID,$url,$title,1,'<base href="'.$webName->url.'" />'.$html0));
    			$urlId = $this->db->query('SELECT id FROM ulrs WHERE str=? and pagetitle=?',array($url,$title))->row();
    			
    			$insData['url_id']= $urlId->id;
    			$insData['brand']=$webName->website;
    			$insData['scrapedate']= date("Y-m-d") ;
    			
    			array_map('trim', $insData);
    			$this->db->insert($this->prjTbl,array_map('TRIM', $insData));
                
                $itemId = $this->db->query('SELECT id FROM '.PRJTBL.' WHERE url_id=?',$urlId->id)->row();
                $insItemData['item_id']= $itemId->id;
                
                $this->db->insert(PRJTBL.'_price',array_map('TRIM', $insItemData));
                
                
			}
			
		}
		redirect('funk/writehtmfiles');
	}

	
	public function jsoncrawler($webId)
		
    {
		$postData = $this->db->query('SELECT id,url FROM links WHERE website_id = ? and scraped=0',$webId)->result(); //assign post data to array $postData
		$limits = $this->crawl_model->getLimits($webId);
		$atrributes = $this->crawl_model->getAttrib($webId);
		$image = $this->crawl_model->getImage($webId);
		$webName = $this->db->query('SELECT url,website FROM websites WHERE id = ?',$webId)->row();
		
		unset($postData['web_id']);
				
    	foreach ($postData as $value) {
    			
    		$url = $webName->url.$value->url;
    		if (!$data = file_get_contents($url)){
                $this->db->query('UPDATE links SET scraped = 2 WHERE id = ?',$value->id);
                
    		}
            else {
    			$html = file_get_html($url);
    			$html0 =preg_replace('/[ ]{2,}|[\t]/',"",str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', '',$html));
    				
    			$insData = array();
    			foreach ($limits as $r) {
    				if(strpos($html0, $r->start) !==FALSE){
    					$insData[$r->tag]=$this->GetBetween($r->start,$r->end,$html0);
    				}
    			}
    			foreach ($image as $r) {
    				foreach($html->find('img#'.$r->tag) as $element)
    		       		$insData[$r->tag]= $element->src;
    			}
    			
    			foreach($html->find('title') as $element)
    	       		$title= str_replace('</title>','',str_replace('<title>', '', $element));
    		  	
    			
    		  	$this->db->query("INSERT INTO `ulrs`( `proj_id`,`str`,`pagetitle`, `status`, `urlhtml`) VALUES (?,?,?,?)",array(PRJID,$url,$title,1,'<base href="'.$webName->url.'" />'.$html0));
    			$ext1 = $this->db->query('SELECT id FROM ulrs WHERE str=? and pagetitle=?',array($url,$title))->row();
    						
    			$insData['url_id']= $ext1->id;
    			$insData['scrapedate']= date("Y-m-d") ;
    			$insData['brand']=$webName->website;
    						
    			array_map('trim', $insData);
    			$this->db->insert($this->prjTbl,array_map('TRIM', $insData));
    			$this->db->query('UPDATE links SET scraped = 1 WHERE id = ?',$value->id);
    		}	
			
			
		}
	
        redirect('funk/writehtmfiles');
    }
	
	function getContent($webId){
		$postData = $this->db->query('SELECT id,url FROM links WHERE website_id = ? and scraped=0',$webId)->result(); //assign post data to array $postData
		$limits = $this->crawl_model->getLimits($webId);
		$atrributes = $this->crawl_model->getAttrib($webId);
		$image = $this->crawl_model->getImage($webId);
		$webName = $this->db->query('SELECT url,website FROM websites WHERE id = ?',$webId)->row();
		
		unset($postData['web_id']);
		
		foreach ($postData as $value) {
		$url = $webName->url.$value->url;
		$html = file_get_html($url);
    			$html0 =preg_replace('/[ ]{2,}|[\t]/',"",str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', '',$html));
    				
    		$this->db->query("INSERT INTO `ulrs`( `str`, `status`, `urlhtml`) VALUES (?,?,?)",array($url,1,$html0));
    			
			}
	}
	
	
	function strip($string,$snip,$secsnip){
		
		$strap = substr($string, strpos($string,$snip) +1);
	  	$sr = strstr($strap,$secsnip,TRUE);
		return $sr;
	}
	function GetBetween($var1="",$var2="",$pool){
		$temp1 = strpos($pool,$var1)+strlen($var1);
		$result = substr($pool,$temp1,strlen($pool));
		$dd=strpos($result,$var2);
		if($dd == 0){
			$dd = strlen($result);
		}
		return substr($result,0,$dd);
	}
	function geturls(){
		if($this->input->post()){
			$url = $this->input->post('url');
			$html = file_get_html($url);
	        foreach($html->find('a') as $element){
	        	$urls[]= $element->href; 
            }
            
	         $data['urls']=$urls;
	         $data['website']= $this->review_model->getWeb();
             $data['menu']=4;
             $data['daysProductivity']=$this->review_model->getProductivity();
			 $this->load->view('get_url',$data);
		}
		else{
			$data['urls']=array();
			 $data['menu']=4;
			 $data['daysProductivity']=$this->review_model->getProductivity();
			$this->load->view('get_url',$data);
		}
	}
    
    
    function manualgeturls(){
         
        $html = '';
        foreach($html->find('a') as $element)
            $urls[]= $element->href; 
         
         $data['urls']=$urls;
         $data['website']= $this->review_model->getWeb();
         $this->load->view('get_url',$data);
       
    }
    
    
	function submiturl(){
		echo "<pre>";
		print_r($this->input->post());
	}
	
	function review($id){
	    $this->ifDoneByCurrUsr($id);
		$conn = $this->review_model->getData($id);
		$data = array('scrap' => $this->review_model->getData($id),
						'report'=>$this->review_model->getReport(),
						'fields'=>$this->review_model->getFields($conn->connected),
						//'fields2'=>$this->review_model->getFields2($conn->connected),
						'form2'=>$this->review_model->getForm2(),
						'currency'=>$this->review_model->getCurr(),
						'pricedata'=>$this->review_model->getPrice($id),
						'daysProductivity'=>$this->review_model->getProductivity(),
						'image'=>$this->review_model->getImg($id),
                        'menu'=>5);
		$this->load->view('review',$data);
	}
	
	public function revsubmit(){
		date_default_timezone_set('Africa/Nairobi');
		$t=time();
		$saa = date("Y-m-d H:i:s",$t);
		$extra = array('rev_by'=>$this->session->userdata('username'),'saangapi'=> $saa);
		$all = $this->input->post()+$extra;
		$this->db->where('id',$this->input->post('id'));
		$this->db->update($this->prjTbl,$all);
		redirect('web/review/'.$this->input->post('id'));
	}
	
	public function submitprice(){
		$this->db->insert($this->prjTbl.'_price',$this->input->post());
		redirect('web/review/'.$this->input->post('item_id'));
	}
	
	function delprice($priceId,$itemId){
		$this->db->query('DELETE FROM '.$this->prjTbl.'_price WHERE id = ?',$priceId);
		redirect('web/review/'.$itemId);
	}
    
    public function delProduct($id)
    {
        $this->db->query('UPDATE '.$this->prjTbl.' SET deleted = 1, del_by=?, del_time=NOW() WHERE id = ?',array($this->session->userdata('username'),$id));
        redirect('web');
    }
    
    public function completed()
    {
        $data = array('products' => $this->review_model->getCompleted(),
                        'report'=>$this->review_model->getReport(),
                        'daysProductivity'=>$this->review_model->getProductivity(),
                       'menu'=>7 );
        $this->load->view('products_done',$data);
    }
	public function scrapeImages($value='')
	{
		
	}
    
    public function extractgrid()
    {
        $data = array('report'=>$this->review_model->getReport(),
                        'daysProductivity'=>$this->review_model->getProductivity(),
                        'menu'=>10,
                        'extract'=>$this->db->get('extract_grid')); 
        $this->load->view('extractGrid',$data);
    }
    
    public function editProdData()
    {   $postData = $this->input->post();
        unset($postData['price_id']);
        unset($postData['item_id']);
        
        $this->db->where('id',$this->input->post('price_id'));
        $this->db->update(PRJTBL.'_price',$postData);
        redirect('web/review/'.$this->input->post('item_id'));
    }
	
	
    
	
	
	
	
	
	
	
	
	
	
    
	public function crawlerBackup()
    {
    	$webId = $this->input->post('web_id');//get the website ID
		$postData = $this->input->post(); //assign post data to array $postData
		$limits = $this->crawl_model->getLimits($webId);
		$atrributes = $this->crawl_model->getAttrib($webId);
		$image = $this->crawl_model->getImage($webId);
		
		unset($postData['web_id']);
		
				
    	foreach ($postData as $key => $value) {
    		
			$url = $value;
	        $html = file_get_html($url);
			$html0 =str_replace("	","",str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', '',$html));
				
			$insData = array();
			foreach ($limits as $r) {
				$insData[$r->tag]=$this->GetBetween($r->start,$r->end,$html0);
			}
			foreach ($image as $r) {
				foreach($html->find('img#'.$r->tag) as $element)
		       		$insData[$r->tag]= $element->src;
			}
			
			echo "<pre>";
			print_r($insData);die;
	          foreach($html->find('title') as $element)
		       $title= str_replace('<title>', '', $element);
			  
			  foreach($html->find('img#image') as $element)
		       $image= $element->src;
			 
			  foreach($html->find('h2') as $element)
		       $name =str_replace('</h2>','',str_replace('<h2 itemprop="name">', '', $element )) ;
			  
			  /*foreach($html->find('h1') as $element)
		       $collection=str_replace('<h1 itemprop="brand">', '', $element );*/
		       $collection = $this->GetBetween('<h1 itemprop="brand">','</h1>',$html);
			  
			  
			  foreach($html->find('.price') as $element)
		       $priceext= $element;
		       		  
			  foreach($html->find('.attribute') as $element){
			  	$stack[] =$element; 
			  }
			  $data['movement']=$stack;
				$string = str_replace("	","",implode('^',$stack));
				$price=$this->GetBetween('<span class="price" itemprop="price">','</span>',$priceext);
				$strap= $this->GetBetween("Strap</strong><br />", "</p>",$string);
				$case= $this->GetBetween("Case</strong><br />", "</p>",$string);
				$cod= $this->GetBetween('"content">  <p>  <b>', "</b>",$string);
				$mvmt_quartz= $this->GetBetween('Movement</span>  </div>  <div class="content">  <p>', "</p>",$string);
				$case_size= substr($name, -6);
				$mvmt_hand ='';
				if(strpos($this->GetBetween('Movement</span>  </div>  <div class="content">  <p>', "</p>",$string),"Swiss Made")==TRUE){$swiss_made="Yes";}else{$swiss_made=NULL;};
				   //$this->load->view('crawler',$data);
				
				$html0 =str_replace("	","",str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', '',$html));
				$this->db->query("INSERT INTO `ulrs`( `str`,`pagetitle`, `status`, `urlhtml`) VALUES (?,?,?,?)",array($url,$title,1,$html0));
				$ext1 = $this->db->query('SELECT id FROM ulrs WHERE str=? and pagetitle=?',array($url,$title))->row();
				
				$scrapdata = array('',$ext1->id,$image,'',$name,$collection,'',$strap,$case,'',$cod,$case_size,$mvmt_quartz,$mvmt_hand,$swiss_made,$price,'','','','','','','','','','','','','','','','','','','','','','','','','');
				
				$this->db->query('INSERT INTO `'.$this->prjTbl.'`(`type`,`url_id`, `image`, `newness`, `name`, `collection`, `scrapedate`, `gender`, `strap_material`, `case_material`, `plating`, `cod`, `case_size`, `mvmt_quartz`, `mvmt_hand`, `swiss_made`, `usd`, `eur`, `eur_index_us`, `gbp`, `gbp_index_us`, `chf`, `chf_index_us`, `aud`, `aud_index_us`, `jpy`, `jpy_index_us`, `cny`, `cny_index_us`, `hkd`, `hkd_index_us`, `krw`, `krw_index_us`, `cad`, `cad_index_us`, `inr`, `inr_index_us`) VALUES (?,?,?,?,?,?,CURDATE(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',$scrapdata);
				
		}
		redirect('web/geturls');
    }
    /*DELETE FROM '.$this->prjTbl.' WHERE brand = "Folli Follie UK";
        DELETE FROM ulrs where str like "%www.follifollie.co.uk%";
        UPDATE links SET scraped = 0 WHERE url like "%www.follifollie.co.uk%";
        */
        
        
}
