<?php
if (!defined('BASEPATH'))	exit ('No direct script access allowed');

class users Extends CI_Controller{
		
		function __construct()
		{
			parent:: __construct();
			$this->load->model('user_model');
            $this->load->library('Auth_AD');
		} 
		
		function index(){
			$this->session->sess_destroy();
			$data['login_error'] = "";
			$this->load->view('login',$data);
			
		}
		public function login(){
			
			$this->form_validation->set_rules('username','Username','trim|required|alphanumeric|xss_clean');
			$this->form_validation->set_rules('password','Password','trim|min_length[2]|xss_clean');
			
			if ($this->form_validation->run() == FALSE){
				$data['login_error'] = "";
				$this->load->view('login',$data);
				
			}
			else{
				$username = strtolower($this->input->post('username'));
                $password = $this->input->post('password');
                
                // check the login
                if($this->auth_ad->login($username, $password))
                {           
                    
                        
                        if ($this->session->userdata('ul_id') == 1){
                            redirect('web');
                            }
                        elseif ($this->session->userdata('ul_id') == 2){
                            redirect('web/geturls');
                        }
                        elseif ($this->session->userdata('ul_id') == ''){
                            $this->auth_ad->logout();
                            $data['login_error']="You are not authorized to access the Good Guides tool";
                       
                            $this->load->view("login",$data);
                        }
                    
                }
                else
                {
                    $this->session->set_flashdata('login_error', TRUE);
                    
                    $data['login_error'] = "Wrong Password";
                    $this->load->view('login',$data);
                }
				//extract ($_POST);
				/*
				$username = strtolower($this->input->post('username'));
								$password = $this->input->post('password');
								
								$user_id = $this->user_model->check_login($username,$password);
								
								
								if (!$user_id){
									//login failed:
									$this->session->set_flashdata('login_error', TRUE);
									
									$data['login_error'] = "Wrong Password";
									$this->load->view('login',$data);
								}
								else{
									//logem in	
									$user = $this->user_model->get_user($user_id);
									
										$this->session->set_userdata(array (
													'logged_in'=> TRUE, 
													'user_id' => $user[0]->user_id,
													'username' => $user[0]->username,
													'user' =>$user[0]->user,
													'ul_id'=>$user[0]->ul_id,
													'project'=>2
													)
													);
										
										if ($user[0]->ul_id == 1){
											redirect('web');
											}
										elseif ($user[0]->ul_id == 2){
											redirect('web/geturls');
										}
										elseif ($user[0]->ul_id == 3){
											redirect('admin');
										}
									}	*/
				
			  }
		}
		
		function create_users(){
			
			$this->form_validation->set_rules('reg_username','Username', 'trim|required|alpha_numeric|min_length[4]|xss_clean' );
			$this->form_validation->set_rules('reg_password', 'Password','required|min_length[6]|xss_clean');
			
			//$this->form_validation->set_rules('confirm_password', 'Password Confirmation','required|matches[reg_password]|xss_clean');
			
			if ($this->form_validation->run() == FALSE){
				//validation hasnt been run or there are validation errors
				$data['users'] = $this->user_model->get_users();	
				$this->load->view('create_users',$data);
				 
			}
			else{
				$username= $this->input->post('reg_username');
				$password= $this->input->post('reg_password');
				
				$this->user_model->create_user($username,$password);		
				$data['users'] = $this->user_model->get_users();	
				$this->load->view('create_users',$data);
				
			}
		}
        
		public function logout()
        {
            // perform the logout
            if($this->session->userdata('logged_in')) 
            {
                $data['name'] = $this->session->userdata('cn');
                $data['username'] = $this->session->userdata('username');
                $data['logged_in'] = true;
                $this->auth_ad->logout();
                $data['login_error']="Successfully logged out";
                $this->load->view("login",$data);
            } 
            else 
            {
                $data['logged_in'] = false;
                $data['login_error']="Successfully logged out";
                $this->load->view("login",$data);
                
            }
            
            // now that the logout is done, you can add code for the next step(s) here
        }
		/*
		function logout(){
					
					$this->session->sess_destroy();
					$data['login_error'] = "";
					$this->load->view('login',$data);
				}*/
		
		
}
	