<?php
/**
 *
 */
class Funk extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this -> load -> model('Funk_model');
        $this -> load -> helper('download');
        $this -> require_login();
        $this -> prjTbl = PRJTBL;
    }

    public function require_login() {
    	//Checks if a user is logged in using domain account
 
        if ($this -> session -> userdata('logged_in') == TRUE) {
            return TRUE;
        } else {
            redirect('users');
        }
    }

    public function limits() {
        $data = array('fields' => $this -> Funk_model -> getFields(), 'websites' => $this -> Funk_model -> getWebsites(), 'pagesource' => '', 'menu' => 2);
        $this -> load -> view('admin/htmlcontent', $data);
    }

    function gethtml() {
        $url = $this -> input -> post('product_url');
        $html = file_get_html($url);
        $html0 = preg_replace('/[ ]{2,}|[\t]/', "", str_replace('<!DOCTYPE html>', '', $html));
        $data = array('fields' => $this -> Funk_model -> getFields(), 'websites' => $this -> Funk_model -> getWebsites(), 'pagesource' => $html0, 'menu' => 2);
        $this -> load -> view('admin/htmlcontent', $data);
        //write_file('./resources/coach.txt', $html0);
    }

    public function set_limit() {

        $post = $this -> input -> post();
        unset($post['website']);
        foreach ($post as $key => $value) {
            if ($value) {
                $kdata = explode('_', $key);
                if ($kdata[0] == 'start') {
                    $insdata = array($kdata[1], $this -> input -> post('website'), $value);
                    $this -> db -> query('INSERT INTO limits(`field_id`,`web_id`,`start`,`active`,`type`) VALUES(?,?,?,1,1)', $insdata);
                }
                if ($kdata[0] == 'end') {
                    $updata = array($value, $kdata[1], $this -> input -> post('website'));
                    $this -> db -> query('UPDATE limits SET end =? WHERE `field_id`=? AND `web_id`=?', $updata);
                }
            }
        }
        redirect('funk/limits');
    }

    function rep() {
        /*$qry = $this->db->select('id','str')
         ->where(array('id'=>'>1187'))
         ->get('ulrs');*/
        $qry = $this -> db -> query('SELECT id,str FROM ulrs WHERE id >2406');
        foreach ($qry->result() as $value) {
            $url = $value -> str;
            $html = file_get_html($url);
            $html0 = preg_replace('/[ ]{2,}|[\t]/', "", str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', '', $html));
            $this -> db -> query('UPDATE ulrs SET urlhtml=? WHERE id =?', array($html0, $value -> id));
        }

    }

    function addus() {
        $brand = "Shinola";
        $data = $this -> db -> query('SELECT watchdata.id,watchdata.usd,ulrs.str FROM watchdata JOIN ulrs ON ulrs.id = watchdata.url_id WHERE watchdata.brand =? and watchdata.id >= 288', $brand) -> result();
        foreach ($data as $row) {
            $this -> db -> query('insert into price(`item_id`,`curr_id`,`amount`,`promo`,`url`) VALUES (?,1,?,"No",?)', array($row -> id, $row -> usd, $row -> str));
        }
        //CleanPriceTable
        $this -> db -> query("DELETE from price WHERE amount is null OR amount = '' and price.promo_price IS NULL");
    }

    function writehtmfiles() {
        foreach ($this->db->get_where('ulrs',array('written'=>0,'proj_id'=>PRJID))->result() as $value) {
            $html = $value -> urlhtml;
            write_file('./htms/' . preg_replace('/[^A-Za-z0-9]/', '_', $value -> str) . '.htm', $html);
            $this -> db -> query('UPDATE ulrs SET written =1 WHERE id =?', $value -> id);
        }

        redirect('web/geturls');
    }

    function sortOtherCurr() {//Takes pounds from watchdata writes to prices
        foreach ($this->db->query("SELECT watchdata.id,watchdata.usd, ulrs.str FROM watchdata JOIN ulrs ON watchdata.url_id= ulrs.id WHERE watchdata.brand='Folli Follie UK'")->result() as $row) {
            //$this->db->insert('price',array('item_id'=>$row->id,'curr_id'=>5,'available'=>'Yes','amount'=>$row->usd,'promo'=>'No','url'=>$row->str));
        }
    }

    function setPrices() {
        $webName = $this -> db -> query('SELECT url,website FROM websites WHERE id = 29') -> row();
        foreach ($this->db->query('select id,str from ulrs where str like "%follifollie%"')->result() as $row) {
            $url = $row -> str;
            $html = file_get_html($url);
            $html0 = preg_replace('/[ ]{2,}|[\t]/', "", str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', '', $html));

            $end = end(explode('/', $url));
            $curr = $this -> db -> get('currency') -> result();
            $item = $this -> db -> query('SELECT id FROM watchdata WHERE url_id=?', array($row -> id)) -> row();
            foreach ($curr as $row) {
                $link = $webName -> url . $row -> follie . $end;
                $garHtml = file_get_html($link);
                if (strpos($garHtml, '<meta name="product_price" content="') !== FALSE) {
                    $price = $this -> GetBetween('<meta name="product_price" content="', '" />', $garHtml);
                    $garData = array('item_id' => $item -> id, 'curr_id' => $row -> id, 'amount' => $price, 'url' => $link);
                    $this -> db -> insert('price', $garData);
                }
            }
        }
    }

    function brands() {
        $data = array('brands' => $this -> Funk_model -> getBrands(), 'menu' => 1, 'product_type' => $this -> Funk_model -> getProductType());
        $this -> load -> view('admin/brands', $data);
    }

    function addBrand() {
        $this -> db -> insert('websites', $this -> input -> post());
        redirect('funk/brands');
    }

    function json_urls() {
        $data = array('menu' => 3, 'websites' => $this -> Funk_model -> getWebsites());
        $this -> load -> view('admin/jsonurls', $data);
    }

    function admin_functions() {
        $data = array('menu' => 6);
        $this -> load -> view('admin/adminFunctions', $data);
    }

    function prs() {
        /*http://pmp.digitaldividedata.com:81/0.PRS/index.php?r=service/create
         project=109
         task=176
         employee=2829
         subtask=484
         date=2015-08-09
         starttime=06:00
         endtime=18:00
         breaktime=01:00
         practisetime=01:30
         unitcompleted=30
         comments=testonly
         office=PNH*/

    }

    function activate($brand) {
        $this -> db -> query("UPDATE " . $this -> prjTbl . " SET reviewed = 2,opened=2 WHERE brand=? AND reviewed=0;", $brand);
    }

    function deactivate($brand) {
        $this -> db -> query("UPDATE " . $this -> prjTbl . " set reviewed = 0, opened = 0 where brand = ? AND reviewed=0;", $brand);

    }

    public function dwnld_img() {
        $brand = 'Pantene';
        if (!file_exists('./images/' . $brand)) {
            mkdir('./images/' . $brand, 0777, TRUE);
        }
        $imgs = $this -> db -> select('id,brand,img_path') -> get_where('' . $this -> prjTbl . '', array('brand' => $brand, 'deleted' => 0)) -> result();
        $i = 1;
        foreach ($imgs as $value) {
            //echo $value->img_path."<br/>";
            $filename = $value -> brand . '' . $i . substr($value -> img_path, -4);
            $data = file_get_contents($value -> img_path);
            // Read the file's contents
            $save = file_put_contents('./images/' . $value -> brand . '/' . $filename, $data);
            $this -> db -> query('UPDATE ' . $this -> prjTbl . ' SET img_filename =? where id=?', array($filename, $value -> id));
            $i++;

        }
    }

    public function update_prod_url($brand = 'Nature Republic') {
        $this -> db -> query('CALL `prod_url_update`(?)', $brand);
    }

    public function update_upc_src($brand = 'Nature Republic') {
        $this -> db -> query('CALL `upc_src_update`(?)', $brand);
    }

    public function extract() {
        $data = array('websites' => $this -> Funk_model -> getWebsites(), 'fields' => $this -> Funk_model -> getFields(), 'pagesource' => '', 'menu' => 9);
        $this -> load -> view('extract', $data);
    }

    public function do_extract() {

    }

    public function csvupload() {
        $this -> Funk_model -> uploadCsvData();
    }

    public function editbrand() {
        $this -> Funk_model -> editBrand();
        redirect('funk/brands');
    }

    public function deletebrand($id) {
        $this -> db -> query('DELETE FROM websites WHERE id = $id');
        redirect('funk/brands');
    }

    public function deflimits() {
        $data = array('brands' => $this -> Funk_model -> getBrandsWithLimits(), 'menu' => 8,);
        $this->load->view('admin/deflimits',$data);
    }
    
    public function editilmit($id)
    {
        $data = array('brands' => $this -> Funk_model -> getBrands(), 'menu' => 8, 'product_type' => $this-> Funk_model -> getProductType(),
                    'webid'=>$id,'info'=>$this->db->query("SELECT l.field_id,l.id,l.`start`,l.`end`,f.`name`,f.tag FROM limits l JOIN `fields` f ON l.field_id = f.id where web_id =?",$id)->result());
        
        $this->load->view('admin/ledit',$data);
    }
    
   public function dellimits($id)
   {
       $brand = $this->db->query('SELECT website FROM websites WHERE id = ?',$id)->row();
       $this->db->query('DELETE FROM limits WHERE web_id = ?',$id);
       $this->db->query('DELETE FROM '.PRJTBL.' WHERE brand = ?;',$brand->website);
       $this->db->query('DELETE FROM ulrs where web_id = ?',$id);
       $this->db->query('DELETE FROM links WHERE website_id = ?',$id);
       redirect('funk/deflimits');
   }
   public function webList()
   {

   }
   
   
   public function scrapeCustom()
   {
       $sizeStart='<th class="label">size</th><td class="data">';
       $sizeEnd = "</td>";
       
       $priceStart = 'class="price"> <span class="price">';
       $priceEnd = "</span>";
      $data = $this->db->query('SELECT * FROM ulrs WHERE str like "%www.bigelowchemists.com%";')->result();
      foreach ($data as $value) {
          $size = $this->GetBetween($sizeStart,$sizeEnd,$value->urlhtml);
          $price = $this->GetBetween($priceStart,$priceEnd,$value->urlhtml);
          if(strpos($value->urlhtml, $sizeStart) !== FALSE){
              $qry1 = $this->db->query("SELECT id FROM goodguides WHERE url_id = ?",$value->id)->row();
              $this->db->query('INSERT INTO goodguides_price (`item_id`,`size`,`price`) VALUES (?,?,?)',array($qry1->id,$size,$price));
          }
      }
   }
   
   function GetBetween($var1="",$var2="",$pool){
        $temp1 = strpos($pool,$var1)+strlen($var1);
        $result = substr($pool,$temp1,strlen($pool));
        $dd=strpos($result,$var2);
        if($dd == 0){
            $dd = strlen($result);
        }
        return substr($result,0,$dd);
    }
    
   public function getScrapData()
   {
       if($this->input->post('website')==''){
           $data = array('brands' => $this->Funk_model->getScrappedBrands(), 'menu' => 9,'ch'=>'');
           $this->load->view('admin/checkData',$data);
       }
       else{
          $data = array('brands' => $this->Funk_model->getScrappedBrands(),'ch'=>'1','data'=>$this->Funk_model->getData(), 'menu' => 9,);
          $this->load->view('admin/checkData',$data);
       }
    }

}
